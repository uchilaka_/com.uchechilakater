Gitter is barely a tool... it's a bash script to make starting to use a git repository less painful

These are quick instructions on setting up the Gitter tool

1. Install a .git repository manager (e.g. Atlassan's sourcetree, github etc.) 
2. Make sure your manager includes .git command line tools
3. Download the Gitter tool and copy the source files into your Project directory
4. Open a Terminal / linux command line
5. Run <gitter_path>/push.sh whenever you are ready to update your remote repository. If it is your first time, you will need your repository path to complete the setup

For ye folks interested in going beyond:
4. Check out the source code to understand how the tool works and mod it for your git workflow
5. Enjoy