#!/bin/sh

## @TODO include code to detect verbose flag -v. For now, program assumes verbose mode is desired

GR_TREE="origin"
GR_BRANCH="master"
GR_HERE="$(pwd)"
GR_VERBOSE=1
LB="======X======O======X=======O=======X"
SLB="------------------------------------"

# Functions
DOBREAK() {
  echo "${LB}${LB}"
}
DOSMBREAK() {
  echo "${SLB}${SLB}"
}
MESSAGE () {
   if [[ $GR_VERBOSE -eq 1 ]]; then
       echo "$1"
   fi
}
SHOW_WD() {
  echo "Current working directory => $GR_HERE"
  DOSMBREAK
}

DOBREAK
echo "Gitter: made with love for the command line. If you are new to the CLI (Command Line Interface), Gitter will hold your hand, and get you comfy with using a GIT repository for code versioning. If you are familiar with GIT - you shouldn't be here! Learn to use GIT itself."
DOBREAK

PUBLISH() {
SHOW_WD
if [[ -d "${GR_HERE}/.git" ]]; then 
    # "Git repo found!"
    # Run command to add new files
    git add .
    if [[ -z $1 ]]; then 
        COMMENT="Nothing to see here. Just keeping everything saved up..."
    else 
        COMMENT="$1"
    fi
    git commit -m "$COMMENT"
    # Push the updates to your remote repository
    ## @TODO add flags for tree and branch when running git push command. For now, we'll assume the tree is always "origin" and the branch is always "master"
    git push origin master
else
    echo "You need to setup a Git repository. First, type the URL for your GIT repo (We'll attempt to help you get setup) followed by [ENTER]: "
    read GR_REPO_URL
    
    # Run the command to initialize a GIT repository in this location
    git init .
    git remote add origin $GR_REPO_URL
    echo "$LB"
    echo "You are all set! When you've done some stuff in your repository, run \"./push.sh\" again to save your changes to your remote repo. Enjoy!"
fi

}
# Run publish method and pass on all arguments
eval "PUBLISH $@"